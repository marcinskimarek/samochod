package pl.sda.zadanie;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CarShop {
    private String nazwa;
    private List<OfertaSamochodowa> listaOfert;

    public CarShop(String nazwa) {
        this.nazwa = nazwa;
        listaOfert = new ArrayList<>();
    }

    public void dodanieWieluOfertSamochodowych(OfertaSamochodowa... ofertaSamochodowa) {
        for (OfertaSamochodowa oferta : ofertaSamochodowa) {
            listaOfert.add(oferta);
        }
    }

    @Override
    public String toString() {
        return "CarShop{" +
                "nazwa='" + nazwa + '\'' +
                ", listaOfert=" + listaOfert +
                '}';
    }

    /**
     * metoda w dwoch wersjach do wyboru standardowo i za pomocą lambdy
     * @return zwraca marki samochodow, ktore sa w sklepie w postaci listy.
     */
    public List<CarBrand> markiKtorePosiadaSklep() {
//        List<CarBrand> markiSamochodow = listaOfert.stream()
//                .map(ofertaSamochodowa -> ofertaSamochodowa.getSamochod().getMarka())
//                .collect(Collectors.toList());
//        return markiSamochodow;
//    }
        List<CarBrand> markiSamochodow = new ArrayList<>();
        for (OfertaSamochodowa oferta : listaOfert) {
            markiSamochodow.add(oferta.getSamochod().getMarka());
        }
        return markiSamochodow;
    }

    /**
     * metoda w dwoch wersjach do wyboru standardowo i za pomocą lambdy
     * @return zwraca samochody, ktore maja naped na 4 koła w postaci listy
     */
    public List<Car> samochodyZNapEdemNaCzteryKola(){
//        List<Car> samochodyZNapedemNaCzteryKola = new ArrayList<>();
//        for(OfertaSamochodowa oferta: listaOfert){
//            if(oferta.getSamochod().getNaped().equals(CarDrive.valueOf("NA_WSZYSTKIE"))){
//                samochodyZNapedemNaCzteryKola.add(oferta.getSamochod());
//            }
//        }
//        return samochodyZNapedemNaCzteryKola;
        List<Car> samochodyZNapedemNaCzteryKola = listaOfert.stream()
                .map(OfertaSamochodowa::getSamochod)
                .filter(listaOfert -> listaOfert.getNaped().equals(CarDrive.valueOf("NA_WSZYSTKIE")))
                .collect(Collectors.toList());
        return samochodyZNapedemNaCzteryKola;
    }

    /**
     * metoda w dwoch wersjach do wyboru standardowo i za pomocą lambdy
     * @param dataOdUzytkownika data w formacie yyyy.MM.dd
     * @return zwraca liste samochodow jezeli sa one "mlodsze" od podanej daty
     */
    public List<Car> samochodyNieStarszeNiz(String dataOdUzytkownika){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
        LocalDate data = LocalDate.parse(dataOdUzytkownika, formatter);
//        List<Car> samochodyNieStarszeNiz = new ArrayList<>();
//        for(OfertaSamochodowa oferta: listaOfert){
//            if(oferta.getSamochod().getDataWyprodukowania().isAfter(data)){
//             samochodyNieStarszeNiz.add(oferta.getSamochod());
//            }
//        }
//        return samochodyNieStarszeNiz;
        return listaOfert.stream()
                .map(OfertaSamochodowa::getSamochod)
                .filter(car -> car.getDataWyprodukowania().isAfter(data))
                .collect(Collectors.toList());
    }

    /**
     * metoda w dwoch wersjach do wyboru standardowo i za pomocą lambdy
     * @param markaOdUzytkownika do podania marka, taka sama jak zadana w clasie enum CarBrand.java
     * @return zwraca liste samochodow o podanej marce
     */
    public List<Car> samochodyDanejMarki(String markaOdUzytkownika){
//        List<Car> samochodyDanejMarki = new ArrayList<>();
//        for (OfertaSamochodowa oferta: listaOfert){
//            if (oferta.getSamochod().getMarka().equals(CarBrand.valueOf(markaOdUzytkownika)))
//            samochodyDanejMarki.add(oferta.getSamochod());
//        }
//        return samochodyDanejMarki;
        return listaOfert.stream()
                .map(OfertaSamochodowa::getSamochod)
                .filter(car -> car.getMarka().equals(CarBrand.valueOf(markaOdUzytkownika)))
                .collect(Collectors.toList());
    }

    /**
     *
     * @return metoda zwraca samochody posortowane po spalaniu rosnaco
     */
    public List<OfertaSamochodowa> samochodyPosortowanePoSpalaniu(){
        return listaOfert.stream()
                .sorted((o1, o2) -> {
                    if (o1.getSamochod().getSpalanie() > o2.getSamochod().getSpalanie()) {
                        return 1;
                    } else if (o2.getSamochod().getSpalanie()>o1.getSamochod().getSpalanie()){
                        return -1;
                    } else{
                        return 0;
                    }
                }).collect(Collectors.toList());
    }

    public List<OfertaSamochodowa> samochodyPosortowanePoMocy(){
        return listaOfert.stream()
                .sorted((o1, o2) -> {
                    if (o1.getSamochod().getMocSilnika()>o2.getSamochod().getMocSilnika()){
                        return 1;
                    }else if (o2.getSamochod().getMocSilnika()<o1.getSamochod().getMocSilnika()){
                        return -1;
                    }else {
                        return 0;
                    }
                }).collect(Collectors.toList());
    }

    /**
     *
     * @return zwraca liste samochodow posortowaną rosnąco wg ceny
     */
    public List<Car> samochodyPosortowanePoCenie(){
        return listaOfert.stream()
                .sorted((o1, o2) -> {
                    if (o1.getCenaZakupu()>o2.getCenaZakupu()){
                        return 1;
                    }else if (o2.getCenaZakupu()>o1.getCenaZakupu()){
                        return -1;
                    }else {
                        return 0;
                    }
                }).map(OfertaSamochodowa::getSamochod)
                .collect(Collectors.toList());
    }

    /**
     *
     * @return zwraca liste ofert posortowanych wg ceny najmu
     */
    public List<OfertaSamochodowa> najtanszeNajmy(){
        return listaOfert.stream()
                .sorted((o1, o2) -> {
                    if(o1.getCenaNajmu()>o2.getCenaNajmu()){
                        return 1;
                    }else if (o2.getCenaNajmu()>o1.getCenaNajmu()){
                        return -1;
                    }else {
                        return 0;
                    }
                }).collect(Collectors.toList());
    }

    /**
     *
     * @return zwraca listę wszystkich samochodów w sklepie
     */
    public List<Car> listaWszystkichSamochodow(){
        return listaOfert.stream()
                .map(OfertaSamochodowa::getSamochod)
                .collect(Collectors.toList());
    }
}

