package pl.sda.zadanie;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        Car c1 = new Car(CarBrand.AUDI, CarType.CABRIO, 5.5, 1.2, 2, 200,
                LocalDate.of(2000, 2, 5), true, CarDrive.PRZOD);
        Car c2 = new Car(CarBrand.BMW, CarType.HATCHBACK, 10.7, 400, 2, 800,
                LocalDate.of(2017, 1, 1), true, CarDrive.TYL);
        Car c3 = new Car(CarBrand.HYUNDAI, CarType.KOMBI, 6.0, 1.2, 6, 96,
                LocalDate.of(2015, 8, 2), false, CarDrive.PRZOD);
        Car c4 = new Car(CarBrand.HONDA, CarType.SUV, 9.3, 3.2, 9, 320,
                LocalDate.of(2005, 5, 29), true, CarDrive.NA_WSZYSTKIE);
        Car c5 = new Car(CarBrand.MERCEDES, CarType.SEDAN, 8.8, 3.5, 4, 250,
                LocalDate.of(2016, 10, 30), false, CarDrive.TYL);
        OfertaSamochodowa o1 = new OfertaSamochodowa(c1, 60000, 500);
        OfertaSamochodowa o2 = new OfertaSamochodowa(c2, 80000, 1000);
        OfertaSamochodowa o3 = new OfertaSamochodowa(c3, 40000, 200);
        OfertaSamochodowa o4 = new OfertaSamochodowa(c4, 55000, 180);
        OfertaSamochodowa o5 = new OfertaSamochodowa(c5, 120000, 1900);
        CarShop shop1 = new CarShop("Sklep samochodowy");
        shop1.dodanieWieluOfertSamochodowych(o1, o2, o3, o4, o5);

//        System.out.println(shop1);
//        System.out.println(shop1.markiKtorePosiadaSklep());
//        System.out.println(shop1.samochodyZNapEdemNaCzteryKola());
//        System.out.println(shop1.samochodyNieStarszeNiz("2016.09.10"));
//        System.out.println(shop1.samochodyDanejMarki("HONDA"));
//        System.out.println(shop1.samochodyPosortowanePoSpalaniu());
//        System.out.println(shop1.samochodyPosortowanePoSpalaniu());
//        System.out.println(shop1.samochodyPosortowanePoCenie());
        System.out.println(shop1.najtanszeNajmy());
    }
}
