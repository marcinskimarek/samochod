package pl.sda.zadanie;

public enum CarBrand {
    AUDI, BMW, MERCEDES, HONDA, MAZDA, HYUNDAI, FORD, FIAT
}
