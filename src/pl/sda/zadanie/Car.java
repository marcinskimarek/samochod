package pl.sda.zadanie;

import java.time.LocalDate;

public class Car {
    private CarBrand marka;
    private CarType typ;
    private double spalanie, wielkoscSilnika;
    private int iloscMiejsc, mocSilnika;
    private LocalDate dataWyprodukowania;
    private boolean automat;
    private CarDrive naped;

    public Car(CarBrand marka, CarType typ, double spalanie, double wielkoscSilnika, int iloscMiejsc, int mocSilnika, LocalDate dataWyprodukowania, boolean automat, CarDrive naped) {
        this.marka = marka;
        this.typ = typ;
        this.spalanie = spalanie;
        this.wielkoscSilnika = wielkoscSilnika;
        this.iloscMiejsc = iloscMiejsc;
        this.mocSilnika = mocSilnika;
        this.dataWyprodukowania = dataWyprodukowania;
        this.automat = automat;
        this.naped = naped;
    }

    public CarBrand getMarka() {
        return marka;
    }

    public void setMarka(CarBrand marka) {
        this.marka = marka;
    }

    public CarType getTyp() {
        return typ;
    }

    public void setTyp(CarType typ) {
        this.typ = typ;
    }

    public double getSpalanie() {
        return spalanie;
    }

    public void setSpalanie(double spalanie) {
        this.spalanie = spalanie;
    }

    public double getWielkoscSilnika() {
        return wielkoscSilnika;
    }

    public void setWielkoscSilnika(double wielkoscSilnika) {
        this.wielkoscSilnika = wielkoscSilnika;
    }

    public int getIloscMiejsc() {
        return iloscMiejsc;
    }

    public void setIloscMiejsc(int iloscMiejsc) {
        this.iloscMiejsc = iloscMiejsc;
    }

    public int getMocSilnika() {
        return mocSilnika;
    }

    public void setMocSilnika(int mocSilnika) {
        this.mocSilnika = mocSilnika;
    }

    public LocalDate getDataWyprodukowania() {
        return dataWyprodukowania;
    }

    public void setDataWyprodukowania(LocalDate dataWyprodukowania) {
        this.dataWyprodukowania = dataWyprodukowania;
    }

    public boolean isAutomat() {
        return automat;
    }

    public void setAutomat(boolean automat) {
        this.automat = automat;
    }

    public CarDrive getNaped() {
        return naped;
    }

    public void setNaped(CarDrive naped) {
        this.naped = naped;
    }

    @Override
    public String toString() {
        return "Car{" +
                "marka=" + marka +
                ", typ=" + typ +
                ", spalanie=" + spalanie +
                ", wielkoscSilnika=" + wielkoscSilnika +
                ", iloscMiejsc=" + iloscMiejsc +
                ", mocSilnika=" + mocSilnika +
                ", dataWyprodukowania=" + dataWyprodukowania +
                ", automat=" + automat +
                ", naped=" + naped +
                '}'+"\n";
    }
}
