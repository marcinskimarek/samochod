package pl.sda.zadanie;

public enum CarType {
    SEDAN, CABRIO, SUV, HATCHBACK, KOMBI
}
