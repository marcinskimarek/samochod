package pl.sda.zadanie;

public class OfertaSamochodowa {
    private Car samochod;
    private int cenaZakupu;
    private int cenaNajmu;

    public OfertaSamochodowa(Car samochod, int cenaZakupu, int cenaNajmu) {
        this.samochod = samochod;
        this.cenaZakupu = cenaZakupu;
        this.cenaNajmu = cenaNajmu;
    }

    @Override
    public String toString() {
        return "OfertaSamochodowa{" +
                "samochod=" + samochod +
                ", cenaZakupu=" + cenaZakupu +
                ", cenaNajmu=" + cenaNajmu +
                '}'+"\n";
    }

    public Car getSamochod() {
        return samochod;
    }

    public void setSamochod(Car samochod) {
        this.samochod = samochod;
    }

    public int getCenaZakupu() {
        return cenaZakupu;
    }

    public void setCenaZakupu(int cenaZakupu) {
        this.cenaZakupu = cenaZakupu;
    }

    public int getCenaNajmu() {
        return cenaNajmu;
    }

    public void setCenaNajmu(int cenaNajmu) {
        this.cenaNajmu = cenaNajmu;
    }
}
